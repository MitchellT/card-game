How To Add New Cards:

	1. Right click the 'Card_Data' folder and select 'Create' > 'Card'
	2. You should see a newly added object named 'New Card (Data Model)'. Click it.
	3. The Inspector should show all of the card attributes such as 'Title', 'Description', etc.
	4. Fill out all of the fields with the desired data. Don't forget to rename the file to the name of the card (spaces should be replaced with underscores)
	5. To add artwork, paste the .png or .jpg file into /Resources/Images
	6. Next, go back to the Card Data Model and click the arrow on the 'Artwork' entry. You should be able to select the image you just added.
	7. Done!

If you wish to preview the card in-game:

	1. Go to Resources/Prefabs and click and drag the CardPrefab object up to the Hiearchy. You want to place the object as a child of one of the rows in the GameBoard object.
	2. You should now see a new card present in one of the rows (in the scene preview).
	3. Select the CardPrefab you just placed in the Hierarchy. The inspector should appear on the right. Expand the section named 'Card (Script)'
	4. Click the arrow on the 'Card Data' entry. You should see your previously created Card Data object. Select it.
	5. Press the 'play' and pan the camera around to preview your card.

