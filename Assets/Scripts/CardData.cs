﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Card (Data Model)", menuName = "Card")]
public class CardData : ScriptableObject
{
    public string title;
    public string type;
    [TextArea(3, 10)]
    public string description;

    public Sprite artwork;

    [Space]
    public int resourceCost;
    public int actionPoints;
    public int damagePoints;
    public int hitPoints;

    public enum CardSet {Set_1, Set_2}
}