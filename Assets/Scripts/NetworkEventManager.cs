﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using static Assets.Scripts.GameConstants;

public class NetworkEventManager
{

    private GameController gameController;

    public NetworkEventManager(GameController gameController)
    {
        this.gameController = gameController;
        RegisterNetworkEvents();
    }

    public void TriggerStateChange(byte stateId, bool masterClientOnly)
    {
        if (masterClientOnly)
        {
            if (PhotonNetwork.IsMasterClient && PhotonNetwork.IsConnected)
            {
                Debug.Log("Triggered state change from master client with stateId: " + stateId);
                PhotonNetwork.RaiseEvent(stateId, null, new RaiseEventOptions { Receivers = ReceiverGroup.All}, SendOptions.SendReliable);
            }
        } else
        {
            Debug.Log("Triggered state change from non-master client with stateId: " + stateId);
            PhotonNetwork.RaiseEvent(stateId, null, new RaiseEventOptions { Receivers = ReceiverGroup.All }, SendOptions.SendReliable);
        }
    }

    public void OnStateChangeNetworkEvent(EventData eventData)
    {
        Debug.Log("Network Event recieved with code: " + eventData.Code);
        switch (eventData.Code)
        {
            case NETWORK_SET_STATE_MATCH_BEGIN:
                gameController.SetState(new BeginMatch(gameController));
                break;
            case NETWORK_SET_STATE_PLAYER_UPKEEP:
                gameController.SetState(new PlayerUpkeep(gameController));
                break;
            case NETWORK_SET_STATE_PLAYER_MAIN:
                gameController.SetState(new PlayerMainPhase(gameController));
                break;
        }
    }

    private void RegisterNetworkEvents()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnStateChangeNetworkEvent;
    }

    private void UnregisterNetworkEvents()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnStateChangeNetworkEvent;
    }
}
