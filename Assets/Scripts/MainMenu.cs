﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviourPunCallbacks
{

    private bool isConnecting = false;

    private const string GAME_VERSION = "0.1";
    private const int MAX_PLAYERS = 2;

    private void Awake() => PhotonNetwork.AutomaticallySyncScene = true;

    public void FindOpponent()
    {
        isConnecting = true;

        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        } else
        {
            PhotonNetwork.GameVersion = GAME_VERSION;
            PhotonNetwork.ConnectUsingSettings();
        }

    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master server.");

        if (isConnecting)
        {
            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log($"Disconnected due to: {cause}");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("No other players are waiting for an opponent. Creating new room...");
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = MAX_PLAYERS });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Succesfully joined a room.");

        int playerCount = PhotonNetwork.CurrentRoom.PlayerCount;

        if (playerCount != MAX_PLAYERS)
        {
            // Display waiting for an opponent message
            Debug.Log("Waiting for an opponent...");
        } else
        {
            Debug.Log("Match is ready to begin.");
        }
    }


    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == MAX_PLAYERS)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;

            Debug.Log("Match is ready to begin.");
            // Display opponent found message

            PhotonNetwork.LoadLevel("MatchScene");
        }
    }

}
