﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;
using static Assets.Scripts.GameConstants;


public class BeginMatch : State
{

    public BeginMatch(GameController gameController) : base(gameController)
    {
    }

    public override IEnumerator Start()
    {
        // Should initialize decks from profile settings.
        // Should draw initial cards for both players.
        // This coroutine should also start the basic animations/actions related to the match starting.
        // will occur. For now, it will simple call this.End() and start the Player's Upkeep Phase

        End();
        return base.Start();
    }

    public override IEnumerator End()
    {
        // Dice rolling mechanic here
        Debug.Log("BeginMatch.End Called");
        gameController.networkEventManager.TriggerStateChange(NETWORK_SET_STATE_PLAYER_UPKEEP, true);
        return base.Start();
    }

} 