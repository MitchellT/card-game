﻿
using Photon.Pun;
using System.Collections;
using static Assets.Scripts.GameConstants;


public abstract class State
{
    protected GameController gameController;
    protected PhotonView photonView;

    public State(GameController gameController)
    {
        this.gameController = gameController;
        photonView = gameController.photonView;
    }

    public virtual IEnumerator Start()
    {
        yield break;
    }

    public virtual IEnumerator End()
    {
        yield break;
    }

    public virtual IEnumerator AddCard(string cardId, string targetRow)
    {
        yield break;
    }

    public virtual IEnumerator MoveCard()
    {
        yield break;
    }
}
