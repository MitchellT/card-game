﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public abstract class StateMachine : MonoBehaviourPun
{

    protected State currentState;

    public void InitNetworkStateChange(byte stateId)
    {
        PhotonNetwork.RaiseEvent(stateId, null, new RaiseEventOptions { Receivers = ReceiverGroup.All }, SendOptions.SendReliable);
    }

    public void SetState(State state)
    {
        currentState = state;
        StartCoroutine(currentState.Start());
    }

}
