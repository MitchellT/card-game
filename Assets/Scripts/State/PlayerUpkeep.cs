﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using static Assets.Scripts.GameConstants;


public class PlayerUpkeep : State
{

    public PlayerUpkeep(GameController gameController) : base(gameController)
    {
    }

    public override IEnumerator Start()
    {
        //Should draw card(s) from stockpiles
        //Should reset actionpoints and resources across all cards
        //Should invoke any effects that occur during upkeep phase.

        End(); //For now we are just skipping to the Main Phase
        return base.Start();
    }

    public override IEnumerator End()
    {
        gameController.networkEventManager.TriggerStateChange(NETWORK_SET_STATE_PLAYER_MAIN, true);
        return base.End();
    }
}
