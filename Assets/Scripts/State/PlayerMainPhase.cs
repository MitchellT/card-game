﻿
using static Assets.Scripts.GameConstants;
using System.Collections;
using UnityEngine;
using Photon.Pun;
using static Assets.Scripts.GameConstants;


public class PlayerMainPhase : State
{
    public PlayerMainPhase(GameController gameController) : base(gameController)
    {
    }

    public override IEnumerator Start()
    {
        Debug.Log("PlayerMainPhase.Start called");
        return base.Start();
    }

    public override IEnumerator End()
    {
        Debug.Log("PlayerMainPhase.End called");
        return base.End();
    }

    public override IEnumerator AddCard(string cardId, string targetRow)
    {
        //TODO: Add logic check using targetRow to see if row is at maximum cards possible
        photonView.RPC("RPC_AddCard", RpcTarget.AllViaServer, new object[] { cardId, targetRow });
        //gameController.RPC_AddCard(cardId, targetRow);
        return base.AddCard(cardId, targetRow);
    }

}
