﻿namespace Assets.Scripts
{
    public static class GameConstants
    {

        /** Game board constants
         * **/
        public const string ROW_PLAYER_NON_UNIT = "playerNonUnitZone";
        public const string ROW_PLAYER_STAGING = "playerStagingZone";
        public const string ROW_PLAYER_FRONTLINE = "playerFrontline";
        public const string ROW_OPPONENT_NON_UNIT = "opponentNonUnitZone";
        public const string ROW_OPPONENT_STAGING = "opponentStagingZone";
        public const string ROW_OPPONENT_FRONTLINE = "opponentFrontline";

        /** Card path constants
         * **/
        public const string CARD_ARGOUX = "Card_Data/Argoux, Resolute Commander";
        public const string CARD_CASTLE_BASTION = "Card_Data/Castle Bastion";
        public const string CARD_CAVALRY = "Card_Data/Cavalry";
        public const string CARD_CITY_MARKET = "Card_Data/City Market";
        public const string CARD_FERTILE_PLAINS = "Card_Data/Fertile Plains";
        public const string CARD_FUTURE_GARRISON = "Card_Data/Future Garrison";
        public const string CARD_HAIL_OF_ARROWS = "Card_Data/Hail of Arrows";
        public const string CARD_LEGION_CAPTAIN = "Card_Data/Legion Captain";
        public const string CARD_PALACE_ARCHIVES = "Card_Data/Palace Archives";
        public const string CARD_PREPARE_FOR_WAR = "Card_Data/Prepare for War";
        public const string CARD_SOLDIER = "Card_Data/Soldier";
        public const string CARD_STORM_THE_FRONT = "Card_Data/Storm the Front";
        public const string CARD_THORALD = "Card_Data/Thorald, King of Last Bastion";
        public const string CARD_WATCHTOWER_GARRISON = "Card_Data/Watchtower Garrison";


        /** Network Event Constants
          **/
        public const int NETWORK_SET_STATE_MATCH_BEGIN = 1;
        public const int NETWORK_SET_STATE_PLAYER_UPKEEP = 2;
        public const int NETWORK_SET_STATE_PLAYER_MAIN = 3;
    }
}
