﻿
using ExitGames.Client.Photon;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Assets.Scripts.GameConstants;

public class GameController : StateMachine
{
    private Vector3 touchStart;
    private Dictionary<string, GridLayoutGroup> gameBoardLayouts = new Dictionary<string, GridLayoutGroup>();

    public Dictionary<string, List<Card>> cardRows = new Dictionary<string, List<Card>>();
    public GameObject cardPrefab;

    //public UnityEvent cardAdded;
    public NetworkEventManager networkEventManager;

    void Start()
    {
        networkEventManager = new NetworkEventManager(this);
        InitializeGameBoard();
        if (PhotonNetwork.IsMasterClient && PhotonNetwork.IsConnected)
        {
            InitNetworkStateChange(NETWORK_SET_STATE_MATCH_BEGIN);
        }
    }

    void Update()    
    {
        HandleCameraPan();
        HandleCameraZoom();
    }

    public void OnAddCard(string cardId, string targetRow)
    {
        currentState.AddCard(cardId, targetRow);
    }

    [PunRPC]
    public void RPC_AddCard(string cardId, string targetRow)
    {
        CardData selectedCard = Resources.Load<CardData>(cardId);
        Debug.Log("Added card " + selectedCard.title + " to " + targetRow);
        cardPrefab.GetComponent<Card>().cardData = selectedCard;

        GameObject cardObject = Instantiate(cardPrefab);
        cardObject.transform.SetParent(gameBoardLayouts[targetRow].transform);
        RefreshGameBoard();

    }

    /** TODO: 
     *      - RemoveCard function
     *      - FindCard function
     * **/

    private void RefreshGameBoard()
    {
        cardRows.Clear();
        foreach (KeyValuePair<string, GridLayoutGroup> cardRowPair in gameBoardLayouts)
        {
            List<Card> currentCardRow = new List<Card>();
            foreach (Transform cardTransform in cardRowPair.Value.transform)
            {
                currentCardRow.Add(cardTransform.gameObject.GetComponent<Card>());
            }
            cardRows.Add(cardRowPair.Key, currentCardRow);
        }
    }

    private void InitializeGameBoard()
    {
        gameBoardLayouts.Add(ROW_PLAYER_NON_UNIT, PhotonView.Find(1).GetComponent<GridLayoutGroup>());
        gameBoardLayouts.Add(ROW_PLAYER_STAGING, PhotonView.Find(2).GetComponent<GridLayoutGroup>());
        gameBoardLayouts.Add(ROW_PLAYER_FRONTLINE, PhotonView.Find(3).GetComponent<GridLayoutGroup>());
        gameBoardLayouts.Add(ROW_OPPONENT_NON_UNIT, PhotonView.Find(4).GetComponent<GridLayoutGroup>());
        gameBoardLayouts.Add(ROW_OPPONENT_STAGING, PhotonView.Find(5).GetComponent<GridLayoutGroup>());
        gameBoardLayouts.Add(ROW_OPPONENT_FRONTLINE, PhotonView.Find(6).GetComponent<GridLayoutGroup>());
        InitializeCardRows();
    }

    private void InitializeCardRows()
    {
        cardRows.Add(ROW_PLAYER_NON_UNIT, new List<Card>());
        cardRows.Add(ROW_PLAYER_STAGING, new List<Card>());
        cardRows.Add(ROW_PLAYER_FRONTLINE, new List<Card>());
        cardRows.Add(ROW_OPPONENT_NON_UNIT, new List<Card>());
        cardRows.Add(ROW_OPPONENT_STAGING, new List<Card>());
        cardRows.Add(ROW_OPPONENT_FRONTLINE, new List<Card>());
    }

    private void HandleCameraPan()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += direction;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log(currentState.GetType().Name);
            OnAddCard(CARD_ARGOUX, ROW_OPPONENT_FRONTLINE);
            OnAddCard(CARD_THORALD, ROW_OPPONENT_FRONTLINE);
            OnAddCard(CARD_WATCHTOWER_GARRISON, ROW_OPPONENT_STAGING);
            OnAddCard(CARD_STORM_THE_FRONT, ROW_PLAYER_FRONTLINE);
            OnAddCard(CARD_CASTLE_BASTION, ROW_PLAYER_FRONTLINE);
        }
    }

    private void HandleCameraZoom()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            Camera.main.orthographicSize -= 20;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Camera.main.orthographicSize += 20;
        }
    }
}
