﻿
using Photon.Pun;
using TMPro;
using UnityEngine.UI;

public class Card : MonoBehaviourPun
{

    public CardData cardData;

    public TextMeshPro titleText;
    public TextMeshPro typeText;
    public TextMeshPro descriptionText;

    public Image artworkImage;

    public TextMeshPro resourcePointsText;
    public TextMeshPro actionPointsText;
    public TextMeshPro damagePointsText;
    public TextMeshPro hitPointsText;

    void Start()
    {
        titleText.text = cardData.title;
        typeText.text = cardData.type;
        descriptionText.text = cardData.description;
        artworkImage.sprite = cardData.artwork;
        resourcePointsText.text = cardData.resourceCost.ToString();
        actionPointsText.text = cardData.actionPoints.ToString();
        damagePointsText.text = cardData.damagePoints.ToString();
        hitPointsText.text = cardData.hitPoints.ToString();
    }

}
